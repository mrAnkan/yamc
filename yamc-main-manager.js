const {app, BrowserWindow} = require('electron')
const path = require('path')
const isDev = require('electron-is-dev');
const Passphrase = require('./main-components/passphrase')
const Settings = require('./main-components/settings')

class YamcMainManager {
  constructor () {
    const dataPath = app.getPath('userData')

    this.unlocked = false
    this.messages = null
    this.passphrase = new Passphrase(dataPath)
    this.settings = new Settings(dataPath)

    this.startupWindow = null
    this.mainWindow = null
    this.workerWindow = null
  }

  createStartupWindow () {
    this.startupWindow = new BrowserWindow({
      width: isDev ? 1800 : 800,
      height: 600,
      webPreferences: { nodeIntegration: true }
    })

    const passphraseIsStored = this.passphrase.passphraseHashFileExists()
    this.startupWindow.loadURL(`file://${__dirname}/views/unlock/unlock.html?passphraseIsStored=${passphraseIsStored}`)
    
    if (isDev) {
      this.startupWindow.webContents.openDevTools()
      this.startupWindow.setMenu(null)
    }
  }

  createMainWindow () {
    this.mainWindow = new BrowserWindow({
      width: isDev ? 1800 : 800,
      height: 600,
      webPreferences: {
        preload: path.join(__dirname, 'views','messages', 'messages-preload.js')
      }
    })
  
    this.mainWindow.loadFile(path.join(__dirname, 'views','messages', 'messages.html'))
    
    if (isDev)
      this.mainWindow.webContents.openDevTools()
  }

  createWorkerWindow() {
    this.workerWindow = new BrowserWindow({
      show: isDev,
      width: isDev ? 1800 : 0,
      height: isDev ? 600 : 0,
      webPreferences: { nodeIntegration: true }
    })
    
    this.workerWindow.loadFile(path.join(__dirname, 'views','worker', 'worker.html'))

    if (isDev)
      this.workerWindow.webContents.openDevTools()
  }

  proceedToMainWindow () {
    if (this.unlocked) {
      this.createMainWindow()
      this.startupWindow.close()
      this.createWorkerWindow()
    }
  }

  async unlock (event, passphrase) {
    try {
      const pass = await this.passphrase.initialize(passphrase)
      const settings = this.settings.initialize(pass.secret)

      event.sender.send('success-response')

      this.unlocked = true
      
      if(!settings.userSettings.imap.user) {
        this.startupWindow.loadURL(`file://${__dirname}/views/settings/settings.html`)
      } else {
       this.proceedToMainWindow()
      }
    } catch (error) {
      if (error.name == 'no-stored-passphrase') {
        event.sender.send('error-response', 'no-stored-passphrase')
      } else {
        event.sender.send('error-response', 'unknown-error-at-unlock')
      }
    }
  }
}

module.exports = YamcMainManager