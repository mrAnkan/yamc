/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { ipcRenderer } = require('electron')
const queryString = require('query-string')

class Unlock {
  constructor(passphraseIsStored) {
    passphraseIsStored = passphraseIsStored === 'true'

    this.passphraseIsStored = passphraseIsStored
    this.divCreatePassphrase = document.getElementById("create-passphrase")
    this.divInputPassphrase = document.getElementById("input-passphrase")
    this.inputSetPassphrase = document.getElementById("yamc-unlock-passphrase")
    this.buttonSetPassphrase = document.getElementById("set-passphrase")
    this.passphrase = document.getElementById('yamc-passphrase')
    this.passphraseAgain = document.getElementById('yamc-repeat-passphrase')
    this.messageWrapper = document.getElementById('message-wrapper')
    this.responseMessage = document.getElementById('response-message')
    
    if (!this.passphraseIsStored) {
      this.divInputPassphrase.classList.add('hidden')
      this.divCreatePassphrase.className = ''
    }
  }

  submitPassphrase() {
    this.buttonSetPassphrase.innerHTML = 
    `
      <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    `
  
    ipcRenderer.invoke('store-passphrase', this.passphrase.value, this.passphraseAgain.value)
  }

  setListeners() {
    this.buttonSetPassphrase.onclick = this.submitPassphrase.bind(this)

    ipcRenderer.on('error-response', async (_event, error) => {
      let errorMessage = 'An unknown error has occurred.'

      if (error == 'no-passphrase-match') {
        this.passphrase.value = ''
        this.passphraseAgain.value = ''
        this.buttonSetPassphrase.innerHTML = 'Set Passphrase'
        errorMessage = 'The provided passphrases did not match.'
      }

      this.messageWrapper.className = ''
      this.responseMessage.innerHTML = errorMessage
    })

    ipcRenderer.on('success-response-store-passphrase', async (_event) => {
      this.passphrase.value = ''
      this.passphraseAgain.value = ''
      this.buttonSetPassphrase.innerHTML = 'Set Passphrase'
      this.responseMessage.innerHTML = 'Successfully stored passphrase'
      
      this.divCreatePassphrase.classList.add('hidden')
      this.divInputPassphrase.className = ''
    })
    
    this.inputSetPassphrase.addEventListener("keyup", (event) => {
      if (event.code === "Enter") {
        event.preventDefault()
        ipcRenderer.invoke('unlock', this.inputSetPassphrase.value)
      }
    }) 
  }
}

window.addEventListener('DOMContentLoaded', () => {
  let query = queryString.parse(global.location.search)
  
  const settings = new Unlock(query.passphraseIsStored)

  settings.setListeners()
})

