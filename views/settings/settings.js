/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { ipcRenderer } = require('electron')

class Settings {
  constructor() {
    this.formSettings = document.getElementById('settings-form')
    this.wrapperSettings = document.getElementById('inner-form-wrapper')
  }

  appendSetting(label, value, type) {
    this.wrapperSettings.appendChild(this.getElementByType(label, value, type))
  }

  getElementByType(label, value, type) {
    const group = document.createElement('div')

    if(!value)
      value = ''

    if(type != 'boolean') {
      group.innerHTML = 
      ` 
        <label for="settings-input-${label}">${label}</label>
        <input type="${type}" name="settings-input-${label}" id="settings-input-${label}" value="${value}" />
      `
    } else {
      group.innerHTML = 
      `
        <label for="settings-input-${label}">${label}</label><br>
        <input type="checkbox" id="settings-input-${label}" name="settings-input-${label}" ${value ? "checked" : ""}>
      `
    }

    return group
  }

  parseSettings(settings) {
    for (const key in settings) {
      if (settings.hasOwnProperty(key)) {
        const element = settings[key];

        if(element && (typeof element == 'object' || typeof element == 'function')) {
          const groupHeader = document.createElement('h2')
          
          groupHeader.innerHTML = key
          this.wrapperSettings.appendChild(groupHeader)

          this.parseSettings(element)
        } else {
          const type = element ? typeof element : 'text'
          this.appendSetting(key, element, type)
        }
      }
    }
  }

  storeSettings(event) {
    event.preventDefault()

    const settings = Object.values(event.target)
      .reduce((prepared, field) => { 
        const name = field.name.replace('settings-input-', '')
        switch (field.type) {
          case 'text':
            prepared[name] = field.value
            break;
          case 'number':
            prepared[name] = parseInt(field.value) 
            break;
          case 'checkbox':
            prepared[name] = field.checked
            break;
        }

        return prepared 
      }, {})
      
      ipcRenderer.invoke('store-user-settings', settings)
  }

  setListeners() {
    this.formSettings.onsubmit = this.storeSettings.bind(this)

    ipcRenderer.on('user-settings', async (_event, settings) => {
      this.parseSettings(settings)
    })

    ipcRenderer.on('success-response', async (event) => {
      console.log(event);
    })
  }
}

window.addEventListener('DOMContentLoaded', () => {
  const settings = new Settings()
  ipcRenderer.invoke('get-user-settings')

  settings.setListeners()
})

