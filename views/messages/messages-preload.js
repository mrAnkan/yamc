/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { ipcRenderer } = require('electron')
const Messages = require('./message-manager.js')

window.yamc = new Messages()

window.addEventListener('DOMContentLoaded', () => {
  window.yamc.initialize()
})

ipcRenderer.on('incoming-messages', async (_event, messages) => {
  console.log(messages)

  for (const message of messages) {
    window.yamc.appendMessage(message)
  }
})