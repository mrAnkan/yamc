const moment = require('moment')

class Messages {

  constructor () {
    this.foldersList = null
    this.messagesList = null
    this.currentMessageHeader = null
    this.currentMessageArea = null 
  }

  initialize () {
    this.foldersList = document.getElementById('folders')
    this.messagesList = document.getElementById('messages')
    this.currentMessageHeader = document.getElementById('current-message-header')
    this.currentMessageArea = document.getElementById('current-message-area')
  }

  appendMessage (message) {
    const li = document.createElement('li')
    const from = message.from.html.substring(31, message.from.html.indexOf('&lt'))
    const div = 
    `
      <div class="message-grid">
        ${from} 
        <span>${message.subject}</span> 
        <span>${moment(message.date).format('YYYY-MM-DD HH:MM')}</span>
      </div>
    `
    li.innerHTML = div

    this.messagesList.appendChild(li)
  }
}

module.exports = Messages

