const { ipcRenderer } = require('electron')
const Messages = require('../../renderer-components/messages.js')

ipcRenderer.invoke('get-user-settings')

ipcRenderer.on('user-settings', async (_event, settings) => {

  const messages = new Messages(settings)
  const list = await messages.getMessages()

  ipcRenderer.invoke('forward-messages-to-renderer', list)
})

