# YAMAC - Yet Another Mail Client
Yet another take on developing a simple mail client. Written in Electron. 

## Key Features
* Setup flow
    * User has to chose passphrase for protecting imap settings
    * User can enter imap settings, ex: username, passphrase and whether or not tls is used 
* Scrypt is used to protect imap settings
* Dark UI
* Background processes for fetching mail for not interfering with ui 
* Cross platform

## Current State
In early development. 
* Chrome Devtools shows up per default
* Setup flow is working, but is currently unstyled
* Login flow is working, although it is also currently unstyled
* First take on fetching data in background process, window for background processing shows upp for debugging purposes
* First take on an inbox layout

![alt text](img/yamc-set-pass.png "Unstyled GUI for Setting Pass")
![alt text](img/yamc-settings.png "Unstyled GUI for Entering Settings")
![alt text](img/yamc-unlock.png "Unstyled GUI for Unlocking Application")
![alt text](img/yamc-with-worker.png "First take on an inbox layout")

## Setup for Development
Clone this branch then run `npm i`

To run the application for development enter following in terminal: `npm run start`

The user data is stored at [Electron userData path](https://www.electronjs.org/docs/latest/api/app#appgetpathname). On Ubuntu this translates to  `/home/user/.config/yamc`