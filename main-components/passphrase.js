/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const crypto = require('./crypto')
const fs = require('fs-extra')
const YamcError = require('./yamc-error')

class Passphrase {
  constructor(path) {
    this.secret = null
    this.hashedSecret = null
    this._path = path
  }

  async initialize(passphrase) {
    if(!this.passphraseHashFileExists())
      throw new YamcError('no-stored-passphrase', 'No stored passphrase exists.')

    const hash = this.getHashForSecretFromFile()
    const [salt, innerSalt, hashedSecretFromFile] = hash.split(":")

    const secretFromUserProvidedPass = await crypto.hash(passphrase, innerSalt)

    const matches = await this.matches(secretFromUserProvidedPass.derivedKey, salt, hashedSecretFromFile)

    if(!matches) 
      throw new YamcError('incorrect-passphrase', 'Provided passphrase does not match the stored passphrase.')

    this.hashedSecret = hashedSecretFromFile
    this.secret = secretFromUserProvidedPass.derivedKey

    return this
  } 

  getHashForSecretFromFile() {
    return this.passphraseHashFileExists() ? fs.readFileSync(`${this._path}/hashedSecret.conf`).toString() : null
  }

  async setPassphrase(passphrase) {
    const combinedSecret = await crypto.hash(passphrase)
    this.secret = combinedSecret.derivedKey

    const combinedHashedSecret = await crypto.hash(this.secret)
    this.hashedSecret = combinedHashedSecret.derivedKey

    fs.writeFileSync(`${this._path}/hashedSecret.conf`, `${combinedHashedSecret.salt}:${combinedSecret.salt}:${this.hashedSecret}`)
  }
  
  passphraseHashFileExists() {
    return fs.existsSync(`${this._path}/hashedSecret.conf`)
  }

  async matches(key, salt, hash) {
    try {
      return await crypto.verify(key, salt, hash)
    } catch (error) {
      return false
    }
  }
}

module.exports = Passphrase
