/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const crypto = require('./crypto')
const fs = require('fs-extra')

class Settings {
  constructor(path) {
    this._path = path
    this.userSettings = null
  }

  initialize(secretKey) {
    if (this.settingsFileExists()) {
      this.userSettings = this.readSettingsFile(secretKey)

    } else {
      this.userSettings = {
        imap: {
          user: null,
          password: null,
          host: null,
          port: 993,
          tls: true,
          authTimeout: 3000
        }
      }

      this.storeSettingsFile(secretKey)
    }

    return this
  }

  storeSettingsFile(secretKey) {
    const encrypted = crypto.encrypt(JSON.stringify(this.userSettings), secretKey.substr(0, 32))
    encrypted.time = new Date()
      
    fs.outputJsonSync(`${this._path}/settings.json`, encrypted)
  }

  settingsFileExists() {
    return fs.existsSync(`${this._path}/settings.json`)
  }

  readSettingsFile(secretKey) {
    const encrypted = fs.readJsonSync(`${this._path}/settings.json`)
    return JSON.parse(crypto.decrypt(encrypted.content, encrypted.salt, secretKey.substr(0, 32)))
  }
}

module.exports = Settings