/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const crypto = require("crypto")

const algorithm = 'aes-256-ctr';
const KEY_LENGTH = 64

var exports = module.exports = {}

exports.hash = async function(passphrase, inputSalt) {
  return new Promise((resolve, reject) => {
      const salt = inputSalt ? inputSalt : crypto.randomBytes(16).toString("hex")

      crypto.scrypt(passphrase, salt, KEY_LENGTH, (err, derivedKey) => {
        if (err) 
          reject(err)
        
        resolve({
          salt: salt,
          derivedKey: derivedKey.toString('hex')
        })
      });
  })
}

exports.verify = async function(passphrase, salt, key) {
  return new Promise((resolve, reject) => {
      crypto.scrypt(passphrase, salt, KEY_LENGTH, (err, derivedKey) => {
          if (err) 
            reject(err)

          resolve(key == derivedKey.toString('hex'))
      });
  })
}

exports.encrypt = function(content, secretKey) {
    const salt = crypto.randomBytes(16)
    const cipher = crypto.createCipheriv(algorithm, secretKey, salt)
    const encrypted = Buffer.concat([cipher.update(content), cipher.final()])

    return {
        salt: salt.toString('hex'),
        content: encrypted.toString('hex')
    }
}

exports.decrypt = function(content, salt, secretKey) {

    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(salt, 'hex'))
    const decrypted = Buffer.concat([decipher.update(Buffer.from(content, 'hex')), decipher.final()])

    return decrypted.toString()
}