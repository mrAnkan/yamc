/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { app, BrowserWindow, ipcMain } = require('electron')
const YamcMainManager = require('./yamc-main-manager.js')

const yamc = new YamcMainManager()

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  yamc.createStartupWindow()
  
  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) yamc.createStartupWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

ipcMain.handle('unlock', async (event, passphrase) => {
  await yamc.unlock(event, passphrase)
})

ipcMain.handle('store-user-settings', async (event, settings) => {
  yamc.settings.userSettings.imap = settings;
  try {
    yamc.settings.storeSettingsFile(yamc.passphrase.secret)
    event.sender.send('success-response')
    yamc.proceedToMainWindow()
  } catch (error) {
    console.log(error);
    event.sender.send('error-response', 'unknown-error')
  }
})

ipcMain.handle('get-user-settings', async (event) => {
  event.sender.send('user-settings', yamc.settings.userSettings)
})

ipcMain.handle('forward-messages-to-renderer', async (_event, messages) => {
  yamc.mainWindow.webContents.send('incoming-messages', messages)
})

ipcMain.handle('store-passphrase', async (event, passphrase, passphraseAgain) => {
  if (passphrase !== passphraseAgain) {
    event.sender.send('error-response', 'no-passphrase-match')
  } else {
    try {

      await yamc.passphrase.setPassphrase(passphrase, passphraseAgain)
      event.sender.send('success-response-store-passphrase')

    } catch (error) {
      event.sender.send('error-response', 'unknown-error')
    }
  }
})