/**
 * YAMC - Yet Another Mail Client
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const imap = require('imap-simple')
const simpleParser = require('mailparser').simpleParser

class Messages {
  constructor(settings) {
    this._config = {
      imap: settings.imap
    }
  }

  async getMessages() {
    return new Promise((resolve, reject) => {
      try {
        imap.connect(this._config)
          .then(async function (connection) {
      
            await connection.openBox('INBOX')
        
            const searchCriteria = [
              'UNSEEN'
            ]
        
            const fetchOptions = {
              bodies: ['HEADER', 'TEXT', ''],
              markSeen: false
            }
        
            const results = await connection.search(searchCriteria, fetchOptions)

            const list = [];
            
            for (const result of results) {
              const combined = result.parts.find(item => item.which == '')
              const parsed = await simpleParser(combined.body)
              list.push(parsed)
            }

            resolve(list)
          })
      } catch (error) {
        reject(error)
      }
    })
  }
}

module.exports = Messages;